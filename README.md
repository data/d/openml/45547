# OpenML dataset: Cardiovascular-Disease-dataset

https://www.openml.org/d/45547

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## Data description

There are 3 types of input features:

* Objective: factual information;
* Examination: results of medical examination;
* Subjective: information given by the patient.

Features:

1. Age | Objective Feature | age | int (days)
2. Height | Objective Feature | height | int (cm) |
3. Weight | Objective Feature | weight | float (kg) |
4. Gender | Objective Feature | gender | categorical code |
5. Systolic blood pressure | Examination Feature | ap_hi | int |
6. Diastolic blood pressure | Examination Feature | ap_lo | int |
7. Cholesterol | Examination Feature | cholesterol | 1: normal, 2: above normal, 3: well above normal |
8. Glucose | Examination Feature | gluc | 1: normal, 2: above normal, 3: well above normal |
9. Smoking | Subjective Feature | smoke | binary |
10. Alcohol intake | Subjective Feature | alco | binary |
11. Physical activity | Subjective Feature | active | binary |
12. Presence or absence of cardiovascular disease | Target Variable | cardio | binary |

All of the dataset values were collected at the moment of medical examination.

**Notes by Uploader to OpenML**

* Gender: 1 - women, 2 - men
* There is no information available on Kaggle where this data was collected.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45547) of an [OpenML dataset](https://www.openml.org/d/45547). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45547/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45547/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45547/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

